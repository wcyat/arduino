void setup()
{
  Serial.begin(9600);
  pinMode(13, OUTPUT);
}
void loop()
{
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // print out the value you read:
  digitalWrite(13, HIGH);
  delay(1023 - sensorValue); // delay in between reads for stability
}
