/*
 Controlling a servo position using a potentiometer (variable resistor)
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
*/

#include <Servo.h>

Servo servo1;  // create servo object to control a servo

int servo1_pin = 6;
int wait = 500;
// int degree = 180;

void setup() {
  Serial.begin(9600);
  servo1.attach(servo1_pin);  // attaches the servo on pin 9 to the servo object
}

void loop() {
  servo1.write(0);
  delay(wait);
  servo1.write(90);
  delay(wait);
  servo1.write(180);
  delay(wait);
  servo1.write(90);
  delay(wait);
}
