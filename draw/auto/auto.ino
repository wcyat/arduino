#include <Servo.h>
#include "inverse-kinematics.hpp"

#define PEN_UP 90
#define PEN_DOWN 10

Servo servo1, servo2, servo3;

const int servo1_pin = 6, servo2_pin = 7, servo3_pin = 5;
const int a = 25, b = 35, c = 45, d = 35, e = 45;

Angle adjust_angles(Angle angles)
{
  // for theta 2 the direction is inverted, so use 180 - angle
  // the servo left is 0 degrees, right is 180 degrees
  angles.theta2 = 180 - angles.theta2;
  return angles;
}

/**
 * @brief draw a cross (intend to)
 */
void draw()
{
  for (int i = 0; i < 2; i++)
  {
    servo1.write(PEN_UP);
    delay(1000);
    // y cannot be < 13, according to tests
    Angle angles = adjust_angles(inverse_kinematics(a, b, c, d, e, 1, i == 0 ? 20 : 40));
    servo2.write(angles.theta1);
    servo3.write(angles.theta2);
    Serial.println(angles.theta1);
    Serial.println(angles.theta2);
    delay(1000);
    servo1.write(PEN_DOWN);
    delay(1000);
    // for the second one can't set y = 13 or degree would be > 180
    angles = adjust_angles(inverse_kinematics(a, b, c, d, e, 15, i == 0 ? 40 : 20));
    servo2.write(angles.theta1);
    servo3.write(angles.theta2);
    Serial.println(angles.theta1);
    Serial.println(angles.theta2);
    delay(1000);
  }
}

void setup()
{
  Serial.begin(9600);
  servo1.attach(servo1_pin);
  servo2.attach(servo2_pin);
  servo3.attach(servo3_pin);
  // set to 90 degrees to see if there's any problems
  servo2.write(90);
  servo3.write(90);
  draw();
}

void loop()
{
//  draw();
}
