#include <math.h>

struct Angle {
  double theta1, theta2;
};

/**
 * @brief convert from radians to degrees
 * @param rad angle in radians
 * @return double 
 */
double radToDeg(double rad) { return rad * 180.0 / M_PI; }

/**
 * @brief get angles from provided parameters
 * @description read lesson notes 3 (p.3 - p.4)
 * @param a base length
 * @param b right first link length
 * @param c right second link length
 * @param d left first link length
 * @param e left second link length
 * @param x x-coordinate of the endpoint
 * @param y y-coordinate of the endpoint (Warning: in the current configuration, i.e. a=25, b=d=35, c=e=45, y < 13 would lead to huge inaccuracies!)
 * @return theta 1 and theta 2 in degrees
 */
Angle inverse_kinematics(double a, double b, double c, double d, double e,
                         double x, double y) {
  // step 1
  double f = sqrt(pow(x, 2) + pow(y, 2));
  double g = sqrt(pow((a - x), 2) + pow(y, 2));
  // step 2
  double phi = atan(y / x);
  double phi2;
  if (x > a) {
    phi2 = M_PI - atan(y / (x - a));
  } else {
    phi2 = atan(y / (a - x));
  }
  double alpha = acos((pow(d, 2) + pow(f, 2) - pow(e, 2)) / (2 * d * f));
  double alpha2 = acos((pow(g, 2) + pow(b, 2) - pow(c, 2)) / (2 * g * b));

  // step 3
  double theta1 = phi + alpha;
  double theta2 = phi2 + alpha2;
  Angle angles;
  angles.theta1 = radToDeg(theta1);
  angles.theta2 = radToDeg(theta2);
  return angles;
}
