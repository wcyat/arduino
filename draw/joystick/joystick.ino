#include <Servo.h>
#include <MsTimer2.h>

Servo servo1, servo2, servo3;

const int servo1_pin = 6;
const int servo2_pin = 7;
const int servo3_pin = 5;
const int left_x_pin = A0;
const int left_y_pin = A1;
const int right_x_pin = A3;
const int right_y_pin = A2;

void monitor()
{
    const int left_x = analogRead(left_x_pin);
    const int left_y = analogRead(left_y_pin);
    const int right_x = analogRead(right_x_pin);
    const int right_y = analogRead(right_y_pin);

    Serial.print("left x ");
    Serial.println(left_x);
    Serial.print("left y ");
    Serial.println(left_y);
    Serial.print("right x ");
    Serial.println(right_x);
    Serial.print("right y ");
    Serial.println(right_y);
}

void setup()
{
    Serial.begin(9600);
    servo1.attach(servo1_pin);
    servo2.attach(servo2_pin);
    servo3.attach(servo3_pin);

    MsTimer2::set(1000, monitor); // 1000ms period
    MsTimer2::start();
}

void loop()
{
    const int left_x = analogRead(left_x_pin);
    const int left_y = analogRead(left_y_pin);
    const int right_x = analogRead(right_x_pin);
    const int right_y = analogRead(right_y_pin);

    if (left_y <= 200) {
      servo1.write(15);
    } else if (left_y >= 900) {
      servo1.write(90);
    }

    servo2.write(map(left_x, 0, 1023, 0, 180));
    servo3.write(map(right_x, 0, 1023, 0, 180));
}
