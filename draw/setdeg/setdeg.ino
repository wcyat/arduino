/*
 Controlling a servo position using a potentiometer (variable resistor)
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
*/

#include <Servo.h>

Servo servo1;  // create servo object to control a servo

int servo1_pin = 5;
int degree = 90;

void setup() {
  Serial.begin(9600);
  servo1.attach(servo1_pin);  // attaches the servo on pin 9 to the servo object
  servo1.write(degree);
}

void loop() {

}