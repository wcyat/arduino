#include <Arduino.h>
/*
top and bottom, each five pins
pin numbers
1 : 4, 2 : 5, 3 : none, 4 : 6, 5 : 7
   __
  |__|
  |__| .

6 : 10, 7 : 11, 8: none, 9 : 12, 10: 13
*/
class sevenseg
{
public:
    const int hello[10][7] = {
        {4, 5, 7, 10, 12, -1}, //H
        {4, 5, 6, 10, 11, -1}, //E
        {5, 10, 11, -1}, //L
        {5, 10, 11, -1}, //L
        {5, 6, 7, 10, 11, 12, -1}}; //O
    const int segleds[8] = {4, 5, 6, 7, 10, 11, 12, 13};
    const void setup()
    {
        for (int i = 0; i < 8; i++)
        {
            pinMode(segleds[i], OUTPUT);
            digitalWrite(segleds[i], HIGH);
        }
    }
    const void reset()
    {
        for (int i = 0; i < 8; i++)
        {
            digitalWrite(segleds[i], HIGH);
        }
    }
    const void set(const int num, const bool on)
    {
        const int state = on ? LOW : HIGH;
        for (int i = 0; i < 7; i++)
        {
            if (hello[num][i] == -1)
            {
                break;
            }
            digitalWrite(hello[num][i], state);
        }
    }
};
