#include "./7segment.hpp"
void setup() {
    sevenseg sg;
    sg.setup();
}
void loop() {
    sevenseg sg;
    for (int i = 0; i < 5; i++) {
        sg.set(i, true);
        delay(1000);
        sg.reset();
    }
}