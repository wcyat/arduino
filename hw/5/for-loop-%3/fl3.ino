void setup() {
    pinMode(13, OUTPUT);
    Serial.begin(9600);
}
void loop() {
    for (int counter = 0; counter < 20; counter++) {
        Serial.println(counter);
        if (!(counter % 3)) {
            digitalWrite(13, HIGH);
            delay(300);
            digitalWrite(13, LOW);
            continue;
        }
        delay(300);
    }
}
