void setup() {
    Serial.begin(9600);
}
bool check(const int val) {
    const int cr[5] = {3, 7, 9, 11, 12};
    for (int i = 0; i < 5; i++) {
        if (cr[i] == val) {
            return true;
        }
    }
    return false;
}
void loop() {
    for (int counter=0; counter<20; counter++) {
        check(counter) && Serial.println(counter);
        delay(1000);
    }
}