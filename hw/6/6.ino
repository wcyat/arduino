#include "7segment.hpp"
//video https://drive.google.com/file/d/1BJh90JRiVu2p-Xpiv0GdT6AwClClNkMB/view?usp=sharing
/*
8 : red
9 : green
*/
const void red() {
    digitalWrite(8, HIGH);
    digitalWrite(9, LOW);
}
const void green() {
    digitalWrite(8, LOW);
    digitalWrite(9, HIGH);
}
const void blinkandcount(const int seconds[2]) {
    for (int i = seconds[1]; i >= seconds[0]; i--)  {
        digitalWrite(9, HIGH);
        set(i, LOW);
        delay(500);
        digitalWrite(9, LOW);
        delay(500);
        set(i, HIGH);
    }
}
void setup() {
    pinMode(8, OUTPUT);
    pinMode(9, OUTPUT);
    for (int i = 0; i < 8; i++) {
        pinMode(segleds[i], OUTPUT);
        digitalWrite(segleds[i], HIGH);
    }
}
void loop () {
    red();
    countdown((const int[2]){0, 5});
    green();
    countdown((const int[2]){4, 9});
    blinkandcount((const int[2]){0, 3});
    reset();
}