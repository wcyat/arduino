#include <Arduino.h>
/*
top and bottom, each five pins
pin numbers
1 : 4, 2 : 5, 3 : none, 4 : 6, 5 : 7
   __
  |__|
  |__| .

6 : 10, 7 : 11, 8: none, 9 : 12, 10: 13
*/
const int n[10][7] = {
    {5, 6, 7, 10, 11, 12, -1}, // 0
    {7, 12, -1},  // 1
    {4, 6, 7, 10, 11, -1}, // 2
    {4, 6, 7, 11, 12, -1},  // 3
    {4, 5, 7, 12, -1}, // 4
    {4, 5, 6, 11, 12, -1}, // 5
    {4, 5, 6, 10, 11, 12, -1}, // 6
    {6, 7, 12, -1}, // 7
    {4, 5, 6, 7, 10, 11, 12}, // 8
    {4, 5, 6, 7, 11, 12}}; //9
const int segleds[8] = {4, 5, 6, 7, 10, 11, 12, 13};
const void reset() {
    for (int i = 0; i < 8; i++) {
        digitalWrite(segleds[i], HIGH);
    }
}
const void set(const int num, const int state) {
    for (int i = 0; i < 7; i++) {
        if (n[num][i] == -1) {
            break;
        }
        digitalWrite(n[num][i], state);
    }
}
const void countdown(const int seconds[2]) {
    for (int i = seconds[1]; i >= seconds[0]; i--) {
        set(i, LOW);
        delay(1000);
        set(i, HIGH);
    }
}