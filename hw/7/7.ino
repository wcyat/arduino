#include "./7segment.hpp"
int last = -1; //any value not in 0 to 9
void setup()
{
    sevenseg sg;
    sg.setup();
    Serial.begin(9600);
}
void loop()
{
    sevenseg sg;
    const int sensorValue = analogRead(A0);
    //considering that using (0, 9) would lead to only one point being 9, I used (0, 10) instead
    int output = map(sensorValue, 0, 1023, 0, 10);
    if (output > 9)
      output = 9;
    if (last != output) {
      sg.reset();
      sg.set(output, true);
      last = output;
    }
}