void setup()
{
    Serial.begin(9600);
}
void loop()
{
    const int testitems[7] = {0, 10, 50, 123, 456, 789, 1023};
    for (int i = 0; i < 7; i++)
    {
        const int y3 = map(testitems[i], 0, 1023, 0, 100);
        const int y4 = map(testitems[i], 0, 1023, 300, 50);
        Serial.print("digital = ");
        Serial.print(testitems[i]);
        Serial.print("; y3 = ");
        Serial.print(y3);
        Serial.print("; y4 = ");
        Serial.println(y4);
    }
    delay(1000);
}