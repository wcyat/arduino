#include <Arduino.h>

char *btParse(const char signal)
{
    char* x;
    switch (signal)
    {
    case '0':
        x = "stop";
        break;
    case 'C':
        x = "grab";
        break;
    case 'D':
        x = "release";
        break;
    case 'E':
        x = "rotateLeft";
        break;
    case 'F':
        x = "rotateRight";
        break;
    case '8':
        x = "forward";
        break;
    case '2':
        x = "backward";
        break;
    case '4':
        x = "left";
        break;
    case '6':
        x = "right";
        break;
    default:
        x = "unknown";
        break;
    }
    return x;
}
