const five = require('johnny-five');
const ledpin = 12;
const board = new five.Board();
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
async function turnon(duration, repeat) {
  const led = new five.Led(ledpin);
  for (let i = 0; i < repeat; i++) {
	led.on();
	await sleep(duration);
	led.stop().off();
	await sleep(duration);
  }
}
board.on('ready', async function() {
	while (true) {
		await turnon(500, 3);
		await turnon(1000, 3);
		await turnon(500, 3);
	}});
