const five = require('johnny-five');
const board = new five.Board();
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
board.on('ready', async function() {
	const red = new five.Led(8);
	const green = new five.Led(12);
	const yellow = new five.Led(13);
	while (true) {
		red.on();
		await sleep(1000);
		yellow.on();
		await sleep(1000);
		red.stop().off()
		yellow.stop().off();
		green.on();
		await sleep(2000);
		green.stop().off();
		yellow.on();
		await sleep(1000);
		yellow.stop().off();
		red.on();
		await sleep(2000);
		red.stop().off();
	}
});