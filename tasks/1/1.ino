int R_LED = 13;
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin R_LED as an output.
  pinMode(R_LED, OUTPUT);
  Serial.begin(9600);	// set the data rate for the serial communication
  digitalWrite(R_LED, LOW);    // turn off the light at the beginning
}

// the loop function runs over and over again forever
void loop() {
    if (Serial.available() > 0) // if a key is activated.
    {		
        char input = Serial.read();  // read the input key
        if (input == 'A') {
	digitalWrite(R_LED, HIGH); // turn the LED on (HIGH is the voltage level)
        }	else if (input == 'B') {
	digitalWrite(R_LED, LOW); // turn the LED off by making the voltage LOW
        }
    }
}
