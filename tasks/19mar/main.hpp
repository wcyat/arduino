#include "./motor.hpp"
const void p1()
{
    if (Serial.available() > 0)
    {
        const char input = Serial.read();
        motor mt;
        Serial.println(input);
        switch (input)
        {
        case 'F':
            mt.forward(5000, 200);
            break;
        case 'B':
            mt.backward(5000, 200);
            break;
        case 'L':
            mt.left(5000, 200);
            break;
        case 'R':
            mt.right(5000, 200);
            break;
        }
    }
}
int last = -1;
const void p2()
{
    const int analogValue = analogRead(A0);
    if (analogValue != last)
    {
        const int output = map(analogValue, 0, 1023, 0, 255);
        Serial.println(output);
        motor mt;
        mt.forward(1000, output);
        last = analogValue;
    }
}
