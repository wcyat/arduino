void setup() {
  pinMode(13, OUTPUT);
  pinMode(8, OUTPUT);
  Serial.begin(9600);  // set the data rate for the serial communication
}
bool on(char i, bool y) { //y: is green?
    auto o = LOW;
    if (i == 'N' || (y && i == 'G') || (!y && i == 'R')) {
        o = HIGH;
    }
    return o;
}
void loop() {
    if (Serial.available() > 0) {// if a key is activated.  
        char input = Serial.read();  // read the input key
        if (input == 'G' || input == 'R' || input == 'N' || input == 'F') {
            //Serial.println(input);
            digitalWrite(8, on(input, true));
            digitalWrite(13, on(input, false));
        }
    }
}