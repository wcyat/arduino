#include "motor.hpp"
const void p1() {
    const int analogValue = analogRead(A0);
    motor mt;
    if (analogValue < 400) {
        mt.left(255);
        return;
    }
    if (analogValue > 600) {
        mt.right(255);
        return;
    }
    mt.reset();
}
const void p2() {
    const int analogValue = analogRead(A0);
    motor mt;
    if (analogValue < 400) {
        mt.left(map(analogValue, 0, 400, 255, 0));
        return;
    }
    if (analogValue > 600) {
        mt.right(map(analogValue, 600, 1023, 0, 255));
        return;
    }
    mt.reset();
}