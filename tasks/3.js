const five = require("johnny-five");
const board = new five.Board();
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
board.on("ready", async () => {
    const led = new five.Led(13);
    while (true) {
        for (let i = 0; i < 20; i++) {
            console.log(i);
            if (i == 13) {
                led.on();
                await sleep(200);
                led.stop().off();
            }
            else {
                await sleep(200);
            }
        }
    }
})