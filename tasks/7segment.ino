/*
top and bottom, each five pins
pin numbers
1 : 4, 2 : 5, 3 : none, 4 : 6, 5 : 7
   __
  |__|
  |__| .

6 : 10, 7 : 11, 8: none, 9 : 12, 10: 13
*/
const int n[10][7] = {
    {5, 6, 7, 10, 11, 12, -1}, // 0
    {7, 12, -1},  // 1
    {4, 6, 7, 10, 11, -1}, // 2
    {4, 6, 7, 11, 12, -1},  // 3
    {4, 5, 7, 12, -1}, // 4
    {4, 5, 6, 11, 12, -1}, // 5
    {4, 5, 6, 10, 11, 12, -1}, // 6
    {6, 7, 12, -1}, // 7
    {4, 5, 6, 7, 10, 11, 12}, // 8
    {4, 5, 6, 7, 11, 12, 13}}; //9 (with a dot)
void setup() {
    const int arr[8] = {4, 5, 6, 7, 10, 11, 12, 13};
    for (int i = 0; i < 8; i++) {
        pinMode(arr[i], OUTPUT);
        digitalWrite(arr[i], HIGH);
    }
}
const void set(const int count, const int state) {
    for (int i = 0; i < 7; i++) {
        if (n[count][i] == -1) {
            break;
        }
        digitalWrite(n[count][i], state);
    }
}
void loop() {
    for (int count = 0; count <= 9; count++) {
        set(count, LOW);
        delay(1000);
        set(count, HIGH);
    }
}
