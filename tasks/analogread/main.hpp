#include "7segment.hpp"
int last = -1;
/**
 * Sensor value and corresponding digit (0 - 9)
 */
const void p1()
{
    sevenseg sg;
    const int val = analogRead(A0);
    int digit = floor(val / 102.3);
    if (digit > 9)
    {
        digit = 9;
    }
    Serial.print("Sensor value: ");
    Serial.print(val);
    Serial.print("; Digit: ");
    Serial.println(digit);
    if (digit != last)
    {
        last = digit;
        sg.reset();
        sg.set(digit, LOW);
    }
}
/**
 * Sersor value and,
 * y1: (0 - 511)
 * y2: (100 - 800)
 * y3: (0 - 100)
 * y4: (300 - 50)
 */
const void p2()
{
    const int val = analogRead(A0);
    const int y1 = map(val, 0, 1023, 0, 511);
    const int y2 = map(val, 0, 1023, 100, 800);
    const int y3 = map(val, 0, 1023, 0, 100);
    const int y4 = map(val, 0, 1023, 300, 50);
    Serial.print("Sersor value: ");
    Serial.print(val);
    Serial.print(", y1: ");
    Serial.print(y1);
    Serial.print(", y2: ");
    Serial.print(y2);
    Serial.print(", y3: ");
    Serial.print(y3);
    Serial.print(", y4: ");
    Serial.println(y4);
    delay(200);
}
/** 
 * Sensor value and,
 * y5: val < 100: 0
 *     val > 800: 10
 *     100 <= val <= 800: (0 - 1000)
*/
const void p3()
{
    const int val = analogRead(A0);
    int y5;
    if (val < 100 || val > 800)
    {
        y5 = val <= 100 ? 0 : 1000;
    }
    else
    {
        y5 = map(val, 100, 800, 0, 1000);
    }
    Serial.print("Sersor value: ");
    Serial.print(val);
    Serial.print(", y5: ");
    Serial.println(y5);
    delay(200);
}