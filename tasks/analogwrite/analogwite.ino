int Pin = 4;
void setup() {
    pinMode(Pin, OUTPUT);
}

void loop() {
    for (int i = 0; i < 256; i++) {
        analogWrite(Pin, i);
        delay(100);
    }
    digitalWrite(Pin, LOW);
    delay(1000);
}