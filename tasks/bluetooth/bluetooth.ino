#include <SoftwareSerial.h>
int RXpin = 11;
int TXpin = 10;
SoftwareSerial mySerial(RXpin, TXpin);
void setup()
{
    // put your setup code here, to run once:
    Serial.begin(9600);
    mySerial.begin(9600);
}
void loop()
{
    // put your main code here, to run repeatedly:
    if (mySerial.available() > 0)
    {
        char input = mySerial.read();
        Serial.println(input);
    }
}