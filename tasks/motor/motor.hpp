#include <Arduino.h>
#include <async.h>

Async asyncEngine = Async();

/**
 * @brief m1l 6, m1r 9, m2l 10, m2r 11;
 *
 */
const int motor_pins[4] = {6, 9, 10, 11};

void motor_reset()
{
    for (int i = 0; i < 4; i++)
    {
        analogWrite(motor_pins[i], 0);
    }
}

class motor
{
public:
    const void set(const int *power)
    {
        analogWrite(m1l, power[0]);
        analogWrite(m1r, power[1]);
        analogWrite(m2l, power[2]);
        analogWrite(m2r, power[3]);
    }
    const void left(const int ms, const int power)
    {
        const int arr[4] = {0, power, power, 0};
        set(arr);
        asyncEngine.setTimeout(motor_reset, ms);
    }
    const void right(const int ms, const int power)
    {
        const int arr[4] = {power, 0, 0, power};
        set(arr);
        asyncEngine.setTimeout(motor_reset, ms);
    }
    const void forward(const int ms, const int power)
    {
        const int arr[4] = {power, 0, power, 0};
        set(arr);
        asyncEngine.setTimeout(motor_reset, ms);
    }
    const void backward(const int ms, const int power)
    {
        const int arr[4] = {0, power, 0, power};
        set(arr);
        asyncEngine.setTimeout(motor_reset, ms);
    }
    const void setup()
    {
        for (int i = 0; i < 4; i++)
        {
            pinMode(pins[i], OUTPUT);
        }
        motor_reset();
    }

private:
    const int m1l = 6, m1r = 9, m2l = 10, m2r = 11;
    const int pins[4] = {6, 9, 10, 11};
};