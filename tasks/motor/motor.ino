#include "./motor.hpp"
#include <SoftwareSerial.h>

int RXpin = 13;
int TXpin = 12;

SoftwareSerial btSerial(RXpin, TXpin);

void setup()
{
    Serial.begin(9600);
    btSerial.begin(9600);
    motor mt;
    mt.setup();
}

void loop()
{
    motor mt;
    if (btSerial.available() > 0)
    {
        const char input = btSerial.read();
        switch (input)
        {
        case '0':
            motor_reset();
            break;
        case 'D':
            motor_reset();
            break;
        case '8':
            mt.forward(5000, 100);
            break;
        case '2':
            mt.backward(5000, 100);
            break;
        case '4':
            mt.left(5000, 100);
            break;
        case '6':
            mt.right(5000, 100);
            break;
        default:
            break;
        }
    }
}